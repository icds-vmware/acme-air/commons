/*
DESCRIPTION: This jenkins pipeline builds a given java
  repository on github and performs the following tasks
  before uploading to artifactory.
STAGES:
  CHECKOUT: gets the git repository
  BUILD: Builds the vRA/vRO packages
  TEST: Run tests on vRO Actions
  SCANS:
    OWASP: Dependency check
    SONAR: Security scan
*/

pipeline {
    agent any

    environment {
        // REPOSITORY_NAME = "${env.GIT_URL.tokenize('/')[3].split('\\.')[0]}"
        // REPOSITORY_OWNER = "${env.GIT_URL.tokenize('/')[2]}"
        GIT_SHORT_REVISION = "${env.GIT_COMMIT[0..7]}"
        DISABLE_DOWNLOAD_PROGRESS_OPTS = '-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn '
        // COVERAGE_EXCLUSIONS = '**/test/**/*,**/itests/**/*,**/*Test*,**/sdk/**/*,**/*.js,**/node_modules/**/*,**/jaxb/**/*,**/wsdl/**/*,**/nces/sws/**/*,**/*.adoc,**/*.txt,**/*.xml'
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '25'))
        disableConcurrentBuilds()
        timestamps()
        disableResume()
        parallelsAlwaysFailFast()
    }
    triggers {
        //  Nightly builds for master branch
        cron(BRANCH_NAME == "master" ? "H H(19-21) * * *" : "")
    }

    tools {
        maven "icds-maven"
    }

    stages {
        stage('Checkout') {
            steps {
                 slackSend color: 'good', message: "STARTED: ${JOB_NAME} ${BUILD_NUMBER} ${BUILD_URL}"

                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                    echo "BUILD_NUMBER = ${BUILD_NUMBER}"
                    echo "BRANCH_NAME = ${BRANCH_NAME}"
                    echo "CHANGE_ID = ${CHANGE_ID}"
                    printenv
                '''
                retry(3){
                    checkout scm
                }
            }
        }
        stage('Build') {
            options {
                timeout(time: 1, unit: 'HOURS')
            }
            steps {
                withMaven(maven: 'icds-maven') {
                    sh 'mvn -U -B -DskipTests clean package deploy'
                }
            }
        }
        stage("Quality") {
            parallel {
                stage('Test') {
                    steps {
                        withMaven(maven: 'icds-maven') {
                            sh 'mvn -U -B test'
                        }
                    }
                }
                stage('SonarQube') {
                    steps {
                        withMaven(maven: 'icds-maven') {
                            sh 'echo SonarQube'
                            sh 'mvn -U -B sonar:sonar -DskipTests'
                        }
                    }
                }
                stage('Dependency Check') {
                    steps {
                        withMaven(maven: 'icds-maven') {
                            sh 'echo Dependency Check'
                            sh 'mvn -U -B org.owasp:dependency-check-maven:check -DskipTests'
                        }
                    }
                }
            }
        }        
        stage('Publish SNAPSHOT (Dev)') {
            when {
                not {
                    branch 'master'
                }
            }
            steps {
                sh 'echo Publish Non-Master'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn deploy -U -B -DskipTests=true -DskipStatic=true -DretryFailedDeploymentCount=10 $DISABLE_DOWNLOAD_PROGRESS_OPTS'
                }
            }
        }
        stage('Publish Release (Master)') {
            when {
                branch 'master'
            }
            steps {
                sh 'echo Publish Master'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn -Drevision=${BUILD_NUMBER} -Pvra74-mss-dev -B -DskipTests=true -DskipStatic=true $DISABLE_DOWNLOAD_PROGRESS_OPTS package deploy'
                }
            }
        }
        stage('Deploy to Dev') {
            when {
                not {
                    branch 'master'
                }
            }
            steps {
                sh 'echo Deploying to the dev environment'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn package deploy vrealize:push -U -Pvra74-mss-dev -B -DskipTests=true -DskipStatic=true $DISABLE_DOWNLOAD_PROGRESS_OPTS'
                }
            }
        }
        stage('Deploy Approval') {
            when {
                branch 'master'
            }
            // timeout(time: 10, unit: "MINUTES") {
                input {
                    message "Should we continue to deploy?"
                    ok "Yes, Approved"
                    submitter "invhariharan"
                    parameters {
                        string(name: 'PERSON', defaultValue: 'invhariharan', description: 'Approver name?')
                    }
                }
                steps {
                    echo "Hello, ${PERSON}, nice to meet you."
                }
            // }
        }
        stage('Deploy to Prod') {
            when {
                branch 'master'
            }
            steps {
                sh 'echo Deploying to the Prod environment'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn -Drevision=${BUILD_NUMBER} -Pvra74-mss-dev -B -DskipTests=true -DskipStatic=true $DISABLE_DOWNLOAD_PROGRESS_OPTS package deploy vrealize:push'
                }
            }
        }
    }
    post {
        always {
             step([$class: 'Mailer',
                 notifyEveryUnstableBuild: true,
                 recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                 sendToIndividuals: true,
                 subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
             ])
        }
        success {
            slackSend color: 'good', message: "SUCCESS: ${JOB_NAME} ${BUILD_NUMBER}"
        }
        failure {
            slackSend color: '#ea0017', message: "FAILURE: ${JOB_NAME} ${BUILD_NUMBER}. See the results here: ${BUILD_URL}"
        }
        unstable {
            slackSend color: '#ffb600', message: "UNSTABLE: ${JOB_NAME} ${BUILD_NUMBER}. See the results here: ${BUILD_URL}"
        }
        aborted {
            slackSend color: '#909090', message: "ABORTED: ${JOB_NAME} ${BUILD_NUMBER}. See the results here: ${BUILD_URL}"
        }
        cleanup {
            echo 'Cleaning up build orkspace...'
            deleteDir()
        }
    }
}
