describe("sample", function() {
    var sample = System.getModule("com.acme.air.commons").sampleSub;
    it("should add two numbers", function() {
        expect(sample(5, 2)).toBe(3);
    });
});
